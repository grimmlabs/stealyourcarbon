## Interface: 90000

## Title: Steal Your Carbon
## Notes: Automatically restock items from vendors and your bank
## Author: Tekkub Stoutwrithe
## Version: 9.0.2.2

## SavedVariablesPerCharacter: StealYourCarbonDB

## LoadManagers: AddonLoader
## X-LoadOn-Merchant: true
## X-LoadOn-Bank: true
## X-LoadOn-InterfaceOptions: Steal Your Carbon

externals\events.lua
externals\gsc.lua
externals\itemid.lua
externals\print.lua

tekKonfig\tekKonfig.xml

StealYourCarbon.lua
Bank.lua
VendorBuyer.lua
WaterUpgrader.lua

Config_Bags.lua
Config_Restock.lua
Config.lua
